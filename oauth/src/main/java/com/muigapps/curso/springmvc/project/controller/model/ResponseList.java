package com.muigapps.curso.springmvc.project.controller.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.muigapps.curso.springmvc.project.model.Category;
import com.muigapps.curso.springmvc.project.model.Client;
import com.muigapps.curso.springmvc.project.model.OrderClient;
import com.muigapps.curso.springmvc.project.model.OrderLine;
import com.muigapps.curso.springmvc.project.model.Product;
import com.muigapps.curso.springmvc.project.model.Street;

@XmlRootElement(name = "response")
@XmlSeeAlso(value = {
	Client.class,
	Street.class,
	Category.class,
	Product.class,
	OrderLine.class,
	OrderClient.class
})
public class ResponseList<T> {
	
	private int page;
	private Long total;
	private int sizePage;
	private int numPage;
	private Boolean state;
	private int code;
	private String message;
	private List<T> data;
	
	
	@XmlAttribute
	public Long getTotal() {
		return total;
	}
	public void setTotal(Long total) {
		this.total = total;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getSizePage() {
		return sizePage;
	}
	public void setSizePage(int sizePage) {
		this.sizePage = sizePage;
	}
	public int getNumPage() {
		return numPage;
	}
	public void setNumPage(int numPage) {
		this.numPage = numPage;
	}
	public Boolean getState() {
		return state;
	}
	public void setState(Boolean state) {
		this.state = state;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@XmlElement
	public List<T> getData() {
		return data;
	}
	public void setData(List<T> data) {
		this.data = data;
	}
	public ResponseList() {
		super();
	}
	public ResponseList(Boolean state, int code, String message, List<T> data) {
		this.state = state;
		this.code = code;
		this.message = message;
		this.data = data;
		this.page = 1;
		this.total = Long.valueOf(data.size());
		this.sizePage = data.size();
		this.numPage = 1;
	}
	
	
	
	

}
