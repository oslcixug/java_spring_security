package com.muigapps.curso.springmvc.project.manager.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;
import com.muigapps.curso.springmvc.project.manager.UserCursoManager;
import com.muigapps.curso.springmvc.project.model.UserCurso;
import com.muigapps.curso.springmvc.project.repository.BaseEntityRepository;
import com.muigapps.curso.springmvc.project.repository.UserCursoRepository;



@Service
public class UserCursoManagerImpl extends BaseManagerImpl<UserCurso,Long> implements UserCursoManager{

	@Autowired
	private UserCursoRepository repository;
	
	private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	
	
	@Override
	protected BaseEntityRepository<UserCurso,Long> getRepository() {
		return repository;
	}
	
	
	@Transactional
	@Modifying
	@Override
	public UserCurso save(UserCurso data) throws CursoSpringMVCException {
		
		if(data.getPassword() != null && !data.getPassword().isEmpty()) {
			data.setPassword(passwordEncoder.encode(data.getPassword()));
		}
		
		return super.save(data);
	}


	@Transactional
	@Modifying
	@Override
	public UserCurso update(Long id, UserCurso data) throws CursoSpringMVCException {
		if(data.getPassword() != null && !data.getPassword().isEmpty()) {
			data.setPassword(passwordEncoder.encode(data.getPassword()));
		} else {
			UserCurso bd = repository.findById(id).orElse(null);
			if(bd != null) {
				data.setPassword(bd.getPassword());
			}
		}
		
		return super.update(id, data);
		
	}
	
	
}
