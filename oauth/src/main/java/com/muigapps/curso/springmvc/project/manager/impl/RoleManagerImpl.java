package com.muigapps.curso.springmvc.project.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.muigapps.curso.springmvc.project.manager.RoleManager;
import com.muigapps.curso.springmvc.project.model.Role;
import com.muigapps.curso.springmvc.project.repository.BaseEntityRepository;
import com.muigapps.curso.springmvc.project.repository.RoleRepository;



@Service
public class RoleManagerImpl extends BaseManagerImpl<Role,Long> implements RoleManager{

	@Autowired
	private RoleRepository repository;

	
	
	@Override
	protected BaseEntityRepository<Role,Long> getRepository() {
		return repository;
	}
	
	
	
}
