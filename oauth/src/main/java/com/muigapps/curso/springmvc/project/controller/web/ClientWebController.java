package com.muigapps.curso.springmvc.project.controller.web;

import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;

import com.muigapps.curso.springmvc.project.controller.model.ResponseList;
import com.muigapps.curso.springmvc.project.exception.CursoNotFoundException;
import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;
import com.muigapps.curso.springmvc.project.manager.ClientManager;
import com.muigapps.curso.springmvc.project.model.Client;
import com.muigapps.curso.springmvc.project.model.Street;

@Controller
@RequestMapping("${url.client.base}")
public class ClientWebController {

	@Autowired
	private ClientManager clientManager;
	
	@Value("${url.client.base}")
	private String urlBase;
	@Value("${url.client.edit}")
	private String urlEdit;
	@Value("${url.client.create}")
	private String urlCreate;
	@Value("${url.client.list}")
	private String urlList;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localResolver;
	
	
	
	
	private final static int SIZE_PAGE = 10;
	
	@RequestMapping(path = {"${url.client.list}"}, method = RequestMethod.GET)
	public String getAll(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		return getAllPage(0, model, request, response);
	}
	
	@RequestMapping(path = {"${url.client.list}{page}", "${url.client.list}{page}/"}, method = RequestMethod.GET)
	public String getAllPage(@PathVariable("page")Integer pageNum, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		ResponseList<Client> page = clientManager.findPage(pageNum, SIZE_PAGE);
		
		model.addAttribute("title", messageSource.getMessage("client.list", null, localResolver.resolveLocale(request)));
		model.addAttribute("clients", page);
		model.addAttribute("menu","client");
		model.addAttribute("fragmentfile", "client/listclient");
		model.addAttribute("fragmentname","listclient");
		
		return "layout/layout";
	}
	
	
	@RequestMapping(path = {"${url.client.view}{id}", "${url.client.view}{id}/"}, method = RequestMethod.GET)
	public String getOne(@PathVariable("id")Long id, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		Client client;
		try {
			client = clientManager.findOne(id);
		} catch (CursoNotFoundException e) {
			client = null;
		}
		
		Object[] args = new Object[1];
		args[0] = (client != null ? client.getName() : "");
		
		model.addAttribute("title",messageSource.getMessage("client.view", args, localResolver.resolveLocale(request)));
		model.addAttribute("client", client);
		model.addAttribute("menu","client");
		model.addAttribute("fragmentfile", "client/viewclient");
		model.addAttribute("fragmentname","viewclient");
		
		
		
		return "layout/layout";
	}
	
	@PreAuthorize("hasAnyRole('WRITER','ADMIN')")
	@RequestMapping(path = {"${url.client.create}"})
	public String create(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		Client client = new Client();
		client.setStreet(new Street());
		
		model.addAttribute("title",messageSource.getMessage("client.create", null, localResolver.resolveLocale(request)));
		model.addAttribute("client", client);
		model.addAttribute("menu","client");
		model.addAttribute("fragmentfile", "client/clientform");
		model.addAttribute("fragmentname","clientform");
		
		
		return "layout/layout";
	}
	
	@PreAuthorize("hasAnyRole('WRITER','ADMIN')")
	@RequestMapping(path = {"${url.client.edit}{id}"})
	public String edit(@PathVariable("id")Long id, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		Boolean save = request.getParameter("save") != null ? Boolean.valueOf(request.getParameter("save")) : null;
		String message = request.getParameter("message") != null ?request.getParameter("message") : null;
		
		Client client;
		try {
			client = clientManager.findOne(id);
			
			if(client.getStreet() == null) {
				client.setStreet(new Street());
			}
		} catch (CursoNotFoundException e) {
			client = null;
		}
		
		Object[] args = new Object[1];
		args[0] = (client != null ? client.getName() : "");
		
		model.addAttribute("title",messageSource.getMessage("client.edit", args, localResolver.resolveLocale(request)));
		model.addAttribute("menu","client");
		model.addAttribute("fragmentfile", "client/clientform");
		model.addAttribute("fragmentname","clientform");
		model.addAttribute("save", save);
		model.addAttribute("message",message);
		model.addAttribute("client", client);
		
		
		
		return "layout/layout";
	}
	
	@RequestMapping(path = {"${url.client.save}"}, method = RequestMethod.POST)
	public String save(Client client, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		Boolean save = false;
		String message = "";
		
		try {
			if(client.getId() == null) {
				client = clientManager.save(client);
				save = true;
				message =  messageSource.getMessage("client.save", null, localResolver.resolveLocale(request));
			} else {
				client = clientManager.update(client.getId(), client);
				save = true;
				message =  messageSource.getMessage("client.save.edit", null, localResolver.resolveLocale(request));
			}
		}catch (CursoSpringMVCException e) {	
			save = false;
			message =  e.getMessageError();
			
		}catch (Exception e) {
			save = false;
			message =  messageSource.getMessage("error.save.generic", null, localResolver.resolveLocale(request));
		}

		
	    if(save) {
	    	return "redirect:"+urlBase+urlEdit+client.getId()+"?save="+save+"&message="+URLEncoder.encode(message);
	    } else {

    		model.addAttribute("client", client);
    		model.addAttribute("save", save);
    		model.addAttribute("message",message);
    		model.addAttribute("client", client);
    		model.addAttribute("menu","client");
    		model.addAttribute("fragmentfile", "client/clientform");
    		model.addAttribute("fragmentname","clientform");
    		
    		
	    	if(client.getId() == null) {
	    		model.addAttribute("title",messageSource.getMessage("client.create", null, localResolver.resolveLocale(request)));
	    	} else {
	    		Object[] args = new Object[1];
	    		args[0] = (client != null ? client.getName() : "");
	    		
	    		model.addAttribute("title",messageSource.getMessage("client.edit", args, localResolver.resolveLocale(request)));
	    	}
	    	

    		return "layout/layout";
	    }
		
	}
	
	@RequestMapping(path = {"${url.client.delete}{id}","${url.client.delete}{id}/"})
	public String delete(@PathVariable("id") Long id, Model model, HttpServletRequest request) {
		try {
			clientManager.delete(id);
			return "redirect:"+urlBase+urlList;
		} catch (CursoSpringMVCException e) {
			return "redirect:"+urlBase+urlList+"?error="+URLEncoder.encode(messageSource.getMessage("error.save", null, localResolver.resolveLocale(request)));
		}
		
		
	}
	
	
}
