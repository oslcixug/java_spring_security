package com.muigapps.curso.springmvc.project.repository.specifications;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.muigapps.curso.springmvc.project.controller.model.FilterCategory;
import com.muigapps.curso.springmvc.project.controller.model.FilterPermission;
import com.muigapps.curso.springmvc.project.controller.model.FilterRole;
import com.muigapps.curso.springmvc.project.controller.model.FilterUserCurso;
import com.muigapps.curso.springmvc.project.model.Category;
import com.muigapps.curso.springmvc.project.model.Role;
import com.muigapps.curso.springmvc.project.model.UserCurso;

public class UserCursoSpecifications implements Specification<UserCurso> {


	private static final long serialVersionUID = -1119531885850044166L;
	
	private FilterUserCurso filter;

	public static UserCursoSpecifications getIntsance(FilterUserCurso filter) {
		return new UserCursoSpecifications(filter);
	}

	public UserCursoSpecifications(FilterUserCurso filter) {
		super();
		this.filter = filter;
	}

	@Override
	public Predicate toPredicate(Root<UserCurso> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		List<Predicate> predicates = new ArrayList<Predicate>();

		
		predicates.add(root.get("deleteDate").isNull());
		
		if (filter != null) {
			if (filter.getUsername() != null && !filter.getUsername().isEmpty()) {
				predicates.add(criteriaBuilder.like(SpecificationsUtils.getExpressionLowerFiled(root.get("username"),criteriaBuilder), SpecificationsUtils.prepareSearchString(filter.getUsername())));	
			}
			if (filter.getEmail() != null && !filter.getEmail().isEmpty()) {
				predicates.add(criteriaBuilder.like(SpecificationsUtils.getExpressionLowerFiled(root.get("email"),criteriaBuilder), SpecificationsUtils.prepareSearchString(filter.getEmail())));	
			}
		}
		
		if (predicates != null && !predicates.isEmpty()) {
			Predicate[] predicatesArray = new Predicate[predicates.size()];
			int pos = 0;
			for (Predicate predicate : predicates) {
				predicatesArray[pos++] = predicate;
			}
			query.distinct(true);
			return criteriaBuilder.and(predicatesArray);
		} else {
			return null;
		}

	}

}
