package com.muigapps.curso.springmvc.project.controller.api;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.MethodNotAllowedException;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.muigapps.curso.springmvc.project.config.ExceptioHandler;
import com.muigapps.curso.springmvc.project.controller.model.FilterClient;
import com.muigapps.curso.springmvc.project.controller.model.Response;
import com.muigapps.curso.springmvc.project.controller.model.ResponseList;
import com.muigapps.curso.springmvc.project.exception.CursoNotFoundException;
import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;
import com.muigapps.curso.springmvc.project.manager.ClientManager;
import com.muigapps.curso.springmvc.project.model.Client;


@RestController
@RequestMapping(path = "/client")
public class ClientController implements ResponseBodyAdvice<Object>{
	
	@Autowired
	private ClientManager clientManager;
	

	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localResolver;

	private Logger logger = LoggerFactory.getLogger(ExceptioHandler.class);
	
	
	@RequestMapping(path = {"/all", "/all/"}, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public ResponseList<Client> findAll(){
		List<Client> clients = clientManager.findAll();
		
		ResponseList<Client> response =  new ResponseList<Client>(true, 200, "ok", clients);
		
		return response;
		
	}
	
	@RequestMapping(path = {"/{id}", "/{id}/"}, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public Response<Client> getOne(@PathVariable("id") Long id) throws CursoNotFoundException{
		Client client = clientManager.findOne(id);
		
		Response<Client> response =  new Response<Client>(true, 200, "ok", client);
		
		return response;
		
	}
	
	
	@RequestMapping(path = {"","/", "/create", "/create/"}, method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public Response<Client> create(@RequestBody Client client) throws CursoSpringMVCException{
		Client clientResult = clientManager.save(client);
		
		Response<Client> response =  new Response<Client>(true, 200, "ok", clientResult);
		
		return response;
	}
	

	@RequestMapping(path = {"/{id}","/{id}/", "/update/{id}", "/update/{id}/"}, method = RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public Response<Client> update(@PathVariable("id") Long id, @RequestBody Client client) throws CursoSpringMVCException{
		Client clientResult = clientManager.update(id, client);
		
		Response<Client> response =  new Response<Client>(true, 200, "ok", clientResult);
		
		return response;
	}
	
	
	@RequestMapping(path = {"/{id}", "/{id}/"}, method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public Response<Boolean> deleteOne(@PathVariable("id") Long id) throws CursoSpringMVCException{
		clientManager.delete(id);
		
		Response<Boolean> response =  new Response<Boolean>(true, 200, "ok", true);
		
		return response;
		
	}
	
	
	@RequestMapping(path = {"/one", "/one/"}, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public Response<Client> getOneparam(@RequestParam(name = "id", required = true) Long id) throws CursoNotFoundException{
		Client client = clientManager.findOne(id);
		
		Response<Client> response =  new Response<Client>(true, 200, "ok", client);
		
		return response;
		
	}
	
	
	@RequestMapping(path = {"/one2", "/one2/"}, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public Response<Client> getOneparam(HttpServletRequest request,HttpServletResponse responseServlet) throws CursoSpringMVCException, CursoNotFoundException{
		
		String idAux = request.getParameter("id");
		
		Long id = null;
		
		try {
			id = Long.valueOf(idAux);
		} catch (Exception e) {
			throw new CursoSpringMVCException(400, messageSource.getMessage("client.id.notvalid", null, localResolver.resolveLocale(request)), null);
		}
		
		Client client = clientManager.findOne(id);
		
		Response<Client> response =  new Response<Client>(true, 200, "ok", client);
		
		return response;
		
	}
	
	@RequestMapping(path = {"/{id}/logic", "/{id}/logic"}, method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public Response<Boolean> deleteLogic(@PathVariable("id") Long id, HttpServletRequest request) throws CursoSpringMVCException, CursoNotFoundException{
		Client client = clientManager.findOne(id);
		
		if(client != null) {
			client.setDeleteDate(new Date());
			
			clientManager.update(id, client);
			Response<Boolean> response =  new Response<Boolean>(true, 200, "ok", true);
			
			return response;
		} else {
			throw new CursoSpringMVCException(402, messageSource.getMessage("client.notfound", null, localResolver.resolveLocale(request)), null);
		}
		
	}
	
	
	@RequestMapping(path = {"/page/{page}/{size}","/page/{page}/{size}/"}, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public ResponseList<Client> page(@PathVariable("page") Integer page, @PathVariable("size") Integer size){
		return clientManager.findPage(page, size);
	}
	
	
	@RequestMapping(path = {"/find","/find/"}, method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseList<Client> find(@RequestBody FilterClient filterClient){
		return clientManager.findAllFilter(filterClient);
	}
	

	@RequestMapping(path = {"/find/{page}/{size}","/find/{page}/{size}/"}, method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseList<Client> find(@PathVariable("page") Integer page, @PathVariable("size") Integer size, @RequestBody FilterClient filterClient){
		return clientManager.findPageFilter(page, size, filterClient);
	}
	
	
	@ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public Response<String> hanlderError(HttpServletRequest request, HttpServletResponse response, MethodArgumentTypeMismatchException e){
		
		Response<String> result = new Response<String>(false, 400, "ERROR",messageSource.getMessage("MethodArgumentTypeMismatchException", null, localResolver.resolveLocale(request)));
		
		return result;
		
	}
	

	
	
	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return false;
	}

	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		return body;
	}
	
	
}
