package com.muigapps.curso.springmvc.project.repository;

import com.muigapps.curso.springmvc.project.model.Role;

public interface RoleRepository extends BaseEntityRepository<Role, Long> {


}
