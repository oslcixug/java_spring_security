package com.muigapps.curso.springmvc.project.config.security;

public class SecurityConstant {

	public static final long TOKEN_EXPIRATION_TIME = 2678400000l;
	public static final String SUPER_SECRET_KEY = "cursoapringsecurity2022";
	public static final String HEADER_AUTHORIZACION_KEY = "Authorization";
	public static final String TOKEN_BEARER_PREFIX = "Bearer";
	public static final String COOKIE_AUTHORIZACION_KEY = "AuthorizationCurso";
	
	
	
}
