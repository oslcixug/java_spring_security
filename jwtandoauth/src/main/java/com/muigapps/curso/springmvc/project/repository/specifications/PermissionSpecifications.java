package com.muigapps.curso.springmvc.project.repository.specifications;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.muigapps.curso.springmvc.project.controller.model.FilterPermission;
import com.muigapps.curso.springmvc.project.model.Permission;

public class PermissionSpecifications implements Specification<Permission> {


	private static final long serialVersionUID = -1119531885850044166L;
	
	private FilterPermission filter;

	public static PermissionSpecifications getIntsance(FilterPermission filter) {
		return new PermissionSpecifications(filter);
	}

	public PermissionSpecifications(FilterPermission filter) {
		super();
		this.filter = filter;
	}

	@Override
	public Predicate toPredicate(Root<Permission> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		List<Predicate> predicates = new ArrayList<Predicate>();

		
		predicates.add(root.get("deleteDate").isNull());
		
		if (filter != null) {
			if (filter.getName() != null && !filter.getName().isEmpty()) {
				predicates.add(criteriaBuilder.like(SpecificationsUtils.getExpressionLowerFiled(root.get("name"),criteriaBuilder), SpecificationsUtils.prepareSearchString(filter.getName())));	
			}
		}
		
		if (predicates != null && !predicates.isEmpty()) {
			Predicate[] predicatesArray = new Predicate[predicates.size()];
			int pos = 0;
			for (Predicate predicate : predicates) {
				predicatesArray[pos++] = predicate;
			}
			query.distinct(true);
			return criteriaBuilder.and(predicatesArray);
		} else {
			return null;
		}

	}

}
