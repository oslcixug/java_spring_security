package com.muigapps.curso.springmvc.project.config.security;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.muigapps.curso.springmvc.project.model.Permission;
import com.muigapps.curso.springmvc.project.model.Role;
import com.muigapps.curso.springmvc.project.model.UserCurso;
import com.muigapps.curso.springmvc.project.repository.UserCursoRepository;

@Component
public class UserDetailsServiceImpl implements UserDetailsService{
	
	@Autowired
	private UserCursoRepository userCursoRepository;
	

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserCurso userCurso = userCursoRepository.findByUsernameOrEmail(username).orElse(null);
		
		
		if(userCurso == null) {
			throw new UsernameNotFoundException("No se ha encontrado el usuario con el username " + username);
		}
		
		UserDetailsCustom user = new UserDetailsCustom(username, userCurso.getPassword(),userCurso.getTwofactor() != null && userCurso.getTwofactor()? UtilsSecurity.getPermissions(userCurso):UtilsSecurity.getPermissionsTwofactor());
		user.setForceTowFactor(userCurso.getTwofactor() == null || !userCurso.getTwofactor()? false:true );
		return user;
	}
	


}
