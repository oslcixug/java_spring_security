package com.muigapps.curso.springmvc.project.controller.web;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.muigapps.curso.springmvc.project.config.security.JWTUsernamePasswordAuthenticationFilter;
import com.muigapps.curso.springmvc.project.model.UserCurso;
import com.muigapps.curso.springmvc.project.repository.UserCursoRepository;

@Controller
public class TwofactorController {

	@Autowired
	private UserCursoRepository userCursoRepository;
	

	@RequestMapping(path = {"/twofactor"}, method = RequestMethod.GET)
	public String loadPage(Model model,String error, HttpServletRequest request, HttpServletResponse response) {
		SimpleGrantedAuthority twoFactor = new SimpleGrantedAuthority("TWOFACTOR");
		if(!SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(twoFactor)) {
			return "redirect:/";
		} else {
			model.addAttribute("error",error);
			return "twofactor";
		}
	}
	
	@RequestMapping(path = {"/twofactor"}, method = RequestMethod.POST)
	public String verify(Model model, HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("codeTwoFactor");
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		UserCurso userBd = userCursoRepository.findByUsernameOrEmail(username).orElse(null);
		
		if(userBd == null) {
			return "redirect:/logout";
		}
		
		if(code.equals(userBd.getCodeTwoFactor())) {
			Calendar now = Calendar.getInstance();
			Calendar finishCode = Calendar.getInstance();
			finishCode.setTime(userBd.getFinishValidCode());
			
			if(now.compareTo(finishCode) <= 0) {
				try {
					JWTUsernamePasswordAuthenticationFilter.createToken(username, true, response);
					return "redirect:/";
				} catch (JsonProcessingException e) {
					return "redirect:/twofactor?error=Error+generando+nuevo+token";
				}
			} else {
				code = JWTUsernamePasswordAuthenticationFilter.getRandomNumberString();
				System.out.println("El código a introducir es: " + code);
				now.add(Calendar.MINUTE, 10);
				userCursoRepository.updateCodeTowFactor(code,now.getTime(), userBd.getUsername());
				return "redirect:/twofactor?error=Código+caducado.+Se+ha+enviado+otro";
			}
			
		} else {
			return "redirect:/twofactor?error=Código+no+valido";
		}
	}
	

}
