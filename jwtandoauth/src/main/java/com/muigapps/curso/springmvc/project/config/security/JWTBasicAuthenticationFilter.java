package com.muigapps.curso.springmvc.project.config.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.muigapps.curso.springmvc.project.config.security.JWTUsernamePasswordAuthenticationFilter.InfoUser;
import com.muigapps.curso.springmvc.project.model.UserCurso;
import com.muigapps.curso.springmvc.project.repository.UserCursoRepository;

import io.jsonwebtoken.Jwts;

public class JWTBasicAuthenticationFilter extends BasicAuthenticationFilter{
	
	private UserCursoRepository userCursoRepository;

	public JWTBasicAuthenticationFilter(AuthenticationManager authenticationManager, UserCursoRepository userCursoRepository) {
		super(authenticationManager);
		this.userCursoRepository = userCursoRepository;
	}
	
	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
		String tokenHeader = req.getHeader(SecurityConstant.HEADER_AUTHORIZACION_KEY);
		String tokenCookie = UtilsSecurity.searchTokenInCookies(req.getCookies());
		
		if ((tokenHeader == null || !tokenHeader.startsWith(SecurityConstant.TOKEN_BEARER_PREFIX)) && tokenCookie == null ) {
			chain.doFilter(req, res);
			return;
		}
		
		UsernamePasswordAuthenticationToken authentication = getAuthentication(tokenHeader == null || tokenHeader.isEmpty() ? SecurityConstant.TOKEN_BEARER_PREFIX+" "+tokenCookie:tokenHeader);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(req, res);
	}
	
	private UsernamePasswordAuthenticationToken getAuthentication(String token) throws ServletException {
		String user = Jwts.parser()
				.setSigningKey(SecurityConstant.SUPER_SECRET_KEY)
				.parseClaimsJws(token.replace(SecurityConstant.TOKEN_BEARER_PREFIX, ""))
				.getBody()
				.getSubject();
		
		UserCurso userCurso = userCursoRepository.findByUsernameOrEmail(user).orElse(null);
		String json  = Jwts.parser()
				.setSigningKey(SecurityConstant.SUPER_SECRET_KEY)
				.parseClaimsJws(token.replace(SecurityConstant.TOKEN_BEARER_PREFIX, ""))
				.getBody().getIssuer();
		InfoUser userDetailsCustom;
		try {
			userDetailsCustom = new ObjectMapper().readValue(json, InfoUser.class);
		} catch (JsonMappingException e) {
			throw new ServletException("No se ha encontrado el usuario");
		} catch (JsonProcessingException e) {
			throw new ServletException("No se ha encontrado el usuario");
		}
		
		if(userCurso == null) {
			throw new ServletException("No se ha encontrado el usuario");
		}
		
		Boolean validTwoFactor =  userDetailsCustom.getTwoFactorValid() != null && userDetailsCustom.getTwoFactorValid();
		
		return new UsernamePasswordAuthenticationTokenTowFactor(user, userCurso, validTwoFactor ? UtilsSecurity.getPermissions(userCurso):UtilsSecurity.getPermissionsTwofactor(),validTwoFactor);
		
		
	}
	

	


}
