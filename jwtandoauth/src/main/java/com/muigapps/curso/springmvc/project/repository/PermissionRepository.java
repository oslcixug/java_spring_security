package com.muigapps.curso.springmvc.project.repository;

import com.muigapps.curso.springmvc.project.model.Permission;

public interface PermissionRepository extends BaseEntityRepository<Permission, Long> {


}
