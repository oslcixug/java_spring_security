package com.muigapps.curso.springmvc.project.manager.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;
import com.muigapps.curso.springmvc.project.manager.UserCursoManager;
import com.muigapps.curso.springmvc.project.model.Role;
import com.muigapps.curso.springmvc.project.model.UserCurso;
import com.muigapps.curso.springmvc.project.repository.BaseEntityRepository;
import com.muigapps.curso.springmvc.project.repository.RoleRepository;
import com.muigapps.curso.springmvc.project.repository.UserCursoRepository;
import com.muigapps.curso.springmvc.project.utils.DefaultPasswordGenerator;



@Service
public class UserCursoManagerImpl extends BaseManagerImpl<UserCurso,Long> implements UserCursoManager{

	@Autowired
	private UserCursoRepository repository;
	
	@Autowired
	private RoleRepository rolRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	
	
	@Override
	protected BaseEntityRepository<UserCurso,Long> getRepository() {
		return repository;
	}
	
	@Transactional
	@Override
	public UserCurso searchOrCreate(String email) {
		UserCurso userBd = repository.findByUsernameOrEmail(email).orElse(null);
		
		if(userBd != null) {
			return userBd;
		}
		
		userBd = new UserCurso();
		userBd.setEmail(email);
		userBd.setEnabled(true);
		userBd.setTwofactor(false);
		userBd.setUsername(email);
		userBd.setCreateDate(new Date());
		String password = DefaultPasswordGenerator.generate(8);
		userBd.setPassword(passwordEncoder.encode(password));
		
		Role role = rolRepository.findByName("READER");
		Set<Role> roles = new HashSet<Role>();
		roles.add(role);
		userBd.setRoles(roles);
		
		userBd = repository.save(userBd);
		
		return userBd;
		
	}
	
	
	@Transactional
	@Modifying
	@Override
	public UserCurso save(UserCurso data) throws CursoSpringMVCException {
		
		if(data.getPassword() != null && !data.getPassword().isEmpty()) {
			data.setPassword(passwordEncoder.encode(data.getPassword()));
		}
		
		return super.save(data);
	}


	@Transactional
	@Modifying
	@Override
	public UserCurso update(Long id, UserCurso data) throws CursoSpringMVCException {
		if(data.getPassword() != null && !data.getPassword().isEmpty()) {
			data.setPassword(passwordEncoder.encode(data.getPassword()));
		} else {
			UserCurso bd = repository.findById(id).orElse(null);
			if(bd != null) {
				data.setPassword(bd.getPassword());
			}
		}
		
		return super.update(id, data);
		
	}
	
	
}
