package com.muigapps.curso.springmvc.project.config.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.muigapps.curso.springmvc.project.controller.model.Response;

public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler{

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		response.addCookie(UtilsSecurity.createCookie(0l, ""));
		if(request.getContentType().equals(MediaType.APPLICATION_JSON_VALUE)) {
			Response<String> responseMess = new Response<String>(false,400,"KO","Credenciales no validas");
			response.setHeader("Content-type", MediaType.APPLICATION_JSON_VALUE);
			response.getWriter().write(new ObjectMapper().writeValueAsString(responseMess)); 
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			
		} else {
			if(response.containsHeader("locked") && response.containsHeader("lockedmin") && response.getHeader("locked").equals("true")) {
				getRedirectStrategy().sendRedirect(request, response, "/login?error=true&locked="+ response.getHeader("lockedmin"));
			} else {
				getRedirectStrategy().sendRedirect(request, response, "/login?error=true");
			}
			
			
		}
		
	}


}
