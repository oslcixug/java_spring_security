package com.muigapps.curso.springmvc.project.config.security;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;

import com.muigapps.curso.springmvc.project.repository.UserCursoRepository;


public class JWTHttpConfigurer extends AbstractHttpConfigurer<JWTHttpConfigurer, HttpSecurity>{
	
	private UserCursoRepository userCursoRepository;
	
	
	
	public JWTHttpConfigurer(UserCursoRepository userCursoRepository) {
		super();
		this.userCursoRepository = userCursoRepository;
	}




	@Override
    public void configure(HttpSecurity http) {
        try {

        	final AuthenticationManager authenticationManager = http.getSharedObject(AuthenticationManager.class);
			
        	http.addFilter(new JWTUsernamePasswordAuthenticationFilter(authenticationManager,userCursoRepository))
			.addFilter(new JWTBasicAuthenticationFilter(authenticationManager,userCursoRepository));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        
        
    }


	

}
