package com.muigapps.curso.springmvc.project.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.muigapps.curso.springmvc.project.model.serializer.ClientSerializer;

@Entity
@Table(name="ORDER_CLIENT")
@XmlRootElement(name = "order")
public class OrderClient  extends BaseEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	@JsonSerialize(using = ClientSerializer.class)
	private Client client;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "orderclient")
	private List<OrderLine> lines;
	private String numberOrder;
	private Date pickUp;
	private Date delivery;
	private String hourPickup;
	private String hourDelivery;
	private Double total;
	private Double base;
	private Double discount;
	private Double discountPercent;
	private Double tax;
	private Double percentTax;
	private Boolean isSuscription;
	@ManyToOne
	private Street pickupLocation;
	@ManyToOne
	private Street deliveryLocation;
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Date getPickUp() {
		return pickUp;
	}
	public void setPickUp(Date pickUp) {
		this.pickUp = pickUp;
	}
	public Date getDelivery() {
		return delivery;
	}
	public void setDelivery(Date delivery) {
		this.delivery = delivery;
	}
	public String getHourPickup() {
		return hourPickup;
	}
	public void setHourPickup(String hourPickup) {
		this.hourPickup = hourPickup;
	}
	public String getHourDelivery() {
		return hourDelivery;
	}
	public void setHourDelivery(String hourDelivery) {
		this.hourDelivery = hourDelivery;
	}
	@XmlElement
	public List<OrderLine> getLines() {
		return lines;
	}
	public void setLines(List<OrderLine> lines) {
		this.lines = lines;
	}
	public Double getBase() {
		return base;
	}
	public void setBase(Double base) {
		this.base = base;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Double getDiscountPercent() {
		return discountPercent;
	}
	public void setDiscountPercent(Double discountPercent) {
		this.discountPercent = discountPercent;
	}
	public Street getPickupLocation() {
		return pickupLocation;
	}
	public void setPickupLocation(Street pickupLocation) {
		this.pickupLocation = pickupLocation;
	}
	public Street getDeliveryLocation() {
		return deliveryLocation;
	}
	public void setDeliveryLocation(Street deliveryLocation) {
		this.deliveryLocation = deliveryLocation;
	}
	public String getNumberOrder() {
		return numberOrder;
	}
	public void setNumberOrder(String numberOrder) {
		this.numberOrder = numberOrder;
	}
	public Boolean getIsSuscription() {
		return isSuscription;
	}
	public void setIsSuscription(Boolean isSuscription) {
		this.isSuscription = isSuscription;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public Double getPercentTax() {
		return percentTax;
	}
	public void setPercentTax(Double percentTax) {
		this.percentTax = percentTax;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((base == null) ? 0 : base.hashCode());
		result = prime * result + ((client == null) ? 0 : client.hashCode());
		result = prime * result + ((delivery == null) ? 0 : delivery.hashCode());
		result = prime * result + ((deliveryLocation == null) ? 0 : deliveryLocation.hashCode());
		result = prime * result + ((discount == null) ? 0 : discount.hashCode());
		result = prime * result + ((discountPercent == null) ? 0 : discountPercent.hashCode());
		result = prime * result + ((hourDelivery == null) ? 0 : hourDelivery.hashCode());
		result = prime * result + ((hourPickup == null) ? 0 : hourPickup.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((isSuscription == null) ? 0 : isSuscription.hashCode());
		result = prime * result + ((lines == null) ? 0 : lines.hashCode());
		result = prime * result + ((numberOrder == null) ? 0 : numberOrder.hashCode());
		result = prime * result + ((pickUp == null) ? 0 : pickUp.hashCode());
		result = prime * result + ((pickupLocation == null) ? 0 : pickupLocation.hashCode());
		result = prime * result + ((total == null) ? 0 : total.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderClient other = (OrderClient) obj;
		if (base == null) {
			if (other.base != null)
				return false;
		} else if (!base.equals(other.base))
			return false;
		if (client == null) {
			if (other.client != null)
				return false;
		} else if (!client.equals(other.client))
			return false;
		if (delivery == null) {
			if (other.delivery != null)
				return false;
		} else if (!delivery.equals(other.delivery))
			return false;
		if (deliveryLocation == null) {
			if (other.deliveryLocation != null)
				return false;
		} else if (!deliveryLocation.equals(other.deliveryLocation))
			return false;
		if (discount == null) {
			if (other.discount != null)
				return false;
		} else if (!discount.equals(other.discount))
			return false;
		if (discountPercent == null) {
			if (other.discountPercent != null)
				return false;
		} else if (!discountPercent.equals(other.discountPercent))
			return false;
		if (hourDelivery == null) {
			if (other.hourDelivery != null)
				return false;
		} else if (!hourDelivery.equals(other.hourDelivery))
			return false;
		if (hourPickup == null) {
			if (other.hourPickup != null)
				return false;
		} else if (!hourPickup.equals(other.hourPickup))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isSuscription == null) {
			if (other.isSuscription != null)
				return false;
		} else if (!isSuscription.equals(other.isSuscription))
			return false;
		if (lines == null) {
			if (other.lines != null)
				return false;
		} else if (!lines.equals(other.lines))
			return false;
		if (numberOrder == null) {
			if (other.numberOrder != null)
				return false;
		} else if (!numberOrder.equals(other.numberOrder))
			return false;
		if (pickUp == null) {
			if (other.pickUp != null)
				return false;
		} else if (!pickUp.equals(other.pickUp))
			return false;
		if (pickupLocation == null) {
			if (other.pickupLocation != null)
				return false;
		} else if (!pickupLocation.equals(other.pickupLocation))
			return false;
		if (total == null) {
			if (other.total != null)
				return false;
		} else if (!total.equals(other.total))
			return false;
		return true;
	}
	
	

	
}
	
	
	
	

