package com.muigapps.curso.springmvc.project.repository.specifications.utils;

import java.text.Normalizer;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class UtilsSpecifications {
	
	
	public static Predicate createLike(CriteriaBuilder criteriaBuilder, Root root, String field, String value) {
		return criteriaBuilder.like(criteriaBuilder.lower(root.get(field)), unaccent(value));
	}
	
	public static String unaccent(String src) {
		if(src != null) {
			return"%"+ Normalizer
					.normalize(src.toLowerCase(), Normalizer.Form.NFD)
					.replaceAll("[^\\p{ASCII}]", "")+"%";
		} else {
			return null;
		}
	}

}
