package com.muigapps.curso.springmvc.project.config.security;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.stereotype.Component;

@Component
public class SearchUserLdap {


	@Autowired
	private LdapTemplate ldapTemplate;
	
	
	public Map<String,String> searchUserLdap(String username){
		List<Map<String,String>> map = ldapTemplate.search("", "uid="+username,new AttributesMapper<Map<String,String>>() {

			@Override
			public Map<String,String> mapFromAttributes(Attributes attributes) throws NamingException {
				Map<String,String> user = new HashMap<String, String>();
				user.put("username",(String)attributes.get("uid").get());
				user.put("password",new String((byte[])attributes.get("userPassword").get()));
				user.put("role",(String)attributes.get("businessCategory").get());
				return user;
			}
		});
		
		return map != null && !map.isEmpty()?map.get(0):null;
	}
}
