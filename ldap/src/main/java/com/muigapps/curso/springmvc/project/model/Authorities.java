package com.muigapps.curso.springmvc.project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="authorities")
@XmlRootElement(name = "authorities")
@IdClass(AuthoritiesId.class)
public class Authorities {

	@Id
	private String username;
	
	@Id
	private String authority;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}
	
	
}
