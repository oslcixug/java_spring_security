package com.muigapps.curso.springmvc.project.repository;

import com.muigapps.curso.springmvc.project.model.Product;

public interface ProductRepository extends BaseEntityRepository<Product, Long> {


}
