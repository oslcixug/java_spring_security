package com.muigapps.curso.springmvc.project.model;

import java.io.Serializable;

import javax.persistence.Id;

public class AuthoritiesId implements Serializable{

	@Id
	private String username;
	
	@Id
	private String authority;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public AuthoritiesId() {
		super();
	}

	public AuthoritiesId(String username, String authority) {
		super();
		this.username = username;
		this.authority = authority;
	}
	
	
	
}
