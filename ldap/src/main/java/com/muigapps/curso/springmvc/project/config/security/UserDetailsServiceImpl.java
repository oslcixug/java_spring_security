package com.muigapps.curso.springmvc.project.config.security;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.muigapps.curso.springmvc.project.repository.RoleRepository;

@Component
public class UserDetailsServiceImpl implements UserDetailsService{
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private SearchUserLdap searchUserLdap;
	

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Map<String,String> map = searchUserLdap.searchUserLdap(username);
		
		
		if(map == null || map.isEmpty() ) {
			throw new UsernameNotFoundException("No se ha encontrado el usuario con el username " + username);
		}
		
		UserBuilder users = User.builder();
		
		return users.username(map.get("username")).password(map.get("password")).authorities(UtilsSecurity.getPermissions(map.get("role"),roleRepository)).build();
	}
	


}
